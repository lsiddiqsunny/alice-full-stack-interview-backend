
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated 
from rest_framework.views import APIView
from rest_framework import status

from django.core import serializers

from .models import SearchUser, Movie
from django.contrib.auth.models import User


apikey='e1e04a26'

class GetAllFavorites(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        data = request.data
        email = data['email']
        try:
            user = User.objects.get(email=email)
            user = SearchUser.objects.filter(user=user)
            if user is None or len(user)==0:
                content = {'error': 'Error occurred!'}
                return Response(content, status=status.HTTP_400_BAD_REQUEST)
            
            favouriteMovies = serializers.serialize("json", user[0].favouriteMovies.all())
            content = {'favouriteMovies': favouriteMovies}
            return Response(content)
        except:
            content = {'error': 'Error occurred!'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)

class AddFavorite(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        data = request.data
        email = data['email']
        try:
            user = User.objects.get(email=email)
            user = SearchUser.objects.filter(user=user)
            if user is None or len(user)==0:
                content = {'error': 'Error occurred!'}
                return Response(content, status=status.HTTP_400_BAD_REQUEST)

            Title = data['Title']
            Year = data['Year'] 
            imdbID = data['imdbID']
            Type = data['Type']

            newMovie = Movie.objects.filter(imdbID=imdbID)
            if newMovie is None or len(newMovie)==0:
                newMovie = Movie(Title=Title,Year=Year,imdbID=imdbID,Type=Type)
                newMovie.save()
            newMovie = Movie.objects.filter(imdbID=imdbID)
            for movie in user[0].favouriteMovies.all():
                
                if movie.imdbID == newMovie[0].imdbID:
                    content = {'error': 'Already in the list!'}
                    return Response(content, status=status.HTTP_400_BAD_REQUEST)

            user[0].favouriteMovies.add(newMovie[0])

            content = {'Message': 'Favourite movie added successfully'}
            return Response(content)

        except:
            content = {'error': 'Error occurred!'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)


class RemoveFavorite(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request, *args, **kwargs):
        data = request.data
        email = data['email']
        try:
            user = User.objects.get(email=email)
            user = SearchUser.objects.filter(user=user)
            if user is None or len(user)==0:
                content = {'error': 'Error occurred!'}
                return Response(content, status=status.HTTP_400_BAD_REQUEST)
            imdbID = data['imdbID']
            

            newMovie = Movie.objects.filter(imdbID=imdbID)
            if newMovie is None or len(newMovie)==0:
                content = {'error': 'No such movie!'}
                return Response(content, status=status.HTTP_400_BAD_REQUEST)

            newMovie = Movie.objects.filter(imdbID=imdbID)
            for movie in user[0].favouriteMovies.all():
                
                if movie.imdbID == newMovie[0].imdbID:
                    user[0].favouriteMovies.remove(movie)
                    content = {'error': 'Successfully removed from the list!'}
                    return Response(content, status=status.HTTP_400_BAD_REQUEST)

            
            content = {'Message': 'Not in the list'}
            return Response(content)

        except:
            content = {'error': 'Error occurred!'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)
        