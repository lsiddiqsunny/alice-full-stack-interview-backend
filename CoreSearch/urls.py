from django.conf.urls import url, include
from django.urls import path
from . import views


urlpatterns = [
    path('getAllFavourites/', views.GetAllFavorites.as_view()),
    path('addFavourite/', views.AddFavorite.as_view()),
    path('removeFavourite/', views.RemoveFavorite.as_view()),

]