from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Movie(models.Model):
    Title = models.CharField(max_length=100)
    Year = models.CharField(max_length=5)
    imdbID = models.CharField(max_length=15)
    Type = models.CharField(max_length=10)
    class Meta:
        ordering = ['Title']

    def __str__(self):
        return self.Title

class SearchUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    favouriteMovies = models.ManyToManyField(Movie,blank=True)

    class Meta:
        ordering = ['user']

    def __str__(self):
        return self.user.username