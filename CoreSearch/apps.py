from django.apps import AppConfig


class CoresearchConfig(AppConfig):
    name = 'CoreSearch'
