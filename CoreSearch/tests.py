from django.test import TestCase

# Create your tests here.
from .models import Movie,SearchUser


class PostTestCase(TestCase):
    def testMovie(self):
        movie = Movie(Title="Movie Title", Year="2021", imdbID="tt123456",Type="movie")
        self.assertEqual(movie.Title, "Movie Title")
        self.assertEqual(movie.Year, "2021")
        self.assertEqual(movie.imdbID, "tt123456")
        self.assertEqual(movie.Type, "movie")