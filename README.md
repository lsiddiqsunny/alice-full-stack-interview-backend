# Movie Search Backend

Home assignment from Alice(<https://getalice.ai/>) for making a Django based custom movie search web application.

Create a python environment and activate it. Then run

```
pip install -r requirements.txt
```

After that run the server at [127.0.0.1:8000](http://127.0.0.1:8000/)](<http://127.0.0.1:8000>) or change server url in Frontend code: **src\_services\user.service.js**.
