
from rest_framework import status
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated 

import json

from CoreSearch.models import SearchUser
class Create(APIView):
    
    def post(self, request, *args, **kwargs):
        data = request.data
        username = data['username']
        password = data['password']
        email = data['email']
        try:
            user = User.objects.get(email=email)
            if user is not None:
                content = {'error': 'User email already taken'}
                return Response(content, status=status.HTTP_400_BAD_REQUEST)
        except:
            user = User.objects.create_user(username, email, password)
            user.save()

            searchUser = SearchUser(user=user)
            searchUser.save()
            content = {'Message': 'User created successfully'}
            return Response(content)



class Login(ObtainAuthToken):
    
    def post(self, request, *args, **kwargs):
        
        data = request.data

       
        email = data['email']
        password = data['password']
        try:
            user = User.objects.get(email=email)
            try:
                authUser = authenticate(username=user.username, password=password)
                if authUser is not None:
                    try:
                        user_token = authUser.auth_token.key
                
                    except:
                        user_token = Token.objects.create(user=authUser).key
                    
                    
                    data = {'token': user_token, 'email': email}
        
                    return Response(data)
                else:
                    content = {'error': 'Wrong Username or Password'}
                    return Response(content, status=status.HTTP_404_NOT_FOUND)
            except:
                content = {'error': 'Wrong Username or Password'}
                return Response(content, status=status.HTTP_404_NOT_FOUND)
            
        except:
            content = {'error': 'Wrong Username or Password'}
            return Response(content, status=status.HTTP_404_NOT_FOUND)

        

class Logout(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        data = request.data
        email = data['email']
        try:
            user = User.objects.get(email=email)
            user.auth_token.delete()
            content = {'Message': 'Logout successfully'}
            return Response(content)
        except:
            content = {'error': 'User email not found'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)
        
        