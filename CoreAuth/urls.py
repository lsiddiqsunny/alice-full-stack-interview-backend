from django.conf.urls import url, include
from django.urls import path
from . import views


urlpatterns = [
    path('create/', views.Create.as_view()),
    path('login/', views.Login.as_view()),
    path('logout/', views.Logout.as_view()),
]