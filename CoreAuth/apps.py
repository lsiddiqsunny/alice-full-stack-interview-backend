from django.apps import AppConfig


class CoreauthConfig(AppConfig):
    name = 'CoreAuth'
